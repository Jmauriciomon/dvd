/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test_dvd;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class DVD {
    
    private String nombre = "DVD";
    private String marca = "SONY";
    private String referencia = "UBP-X700";
    private String calidad = "4K";
    private double precio = 699.900;
    int opcion;
    Scanner leer = new Scanner(System.in);
    

    public DVD(String nombre, String marca, String referencia, String calidad, double precio) {
        this.nombre = "DVD";
        this.marca = "SONY";
        this.referencia = "UBP-X700";
        this.calidad = "4K";
        this.precio = 699.900;
    }

    public DVD() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public void prender(){
        System.out.println("El "+nombre+" "+marca+" "+referencia+" "+calidad+" está encendido");
    }
    
    public void abrir(){
        System.out.println("Bandeja abierta, inserte la pelicula");
    }
    
    public void reproducir(){
        System.out.println("Reproduciendo >");
    }
    
    public void pausar(){
        System.out.println("Pausado >/=");
    }
    
    public void retroceder(){
        do{
        System.out.println("Para retroceder seleccione la velocidad a retroceder: ");
        System.out.println("Para retroceder a una velocidad presione 1");
        System.out.println("Para retroceder a dos velocidades presione 2");
        System.out.println("Para retroceder a tres velocidades presione 3");
        switch(opcion){
            case 1: System.out.println("<");
                break;
            case 2: System.out.println("<<");
                break;
            case 3: System.out.println("<<<");    
        }
        }while(opcion!=3); 
    }
    
    public void adelantar(){
        do{
        System.out.println("Para adelantar seleccione la velocidad a adelantar: ");
        System.out.println("Para adelantar a una velocidad presione 1");
        System.out.println("Para adelantar a dos velocidades presione 2");
        System.out.println("Para adelantar a tres velocidades presione 3");
        switch(opcion){
            case 1: System.out.println(">");
                break;
            case 2: System.out.println(">>");
                break;
            case 3: System.out.println(">>>");    
        }
        }while(opcion!=3); 
        
    }
    
    public void cerrar(){
        System.out.println("Bandeja cerrada");
    }
    
    public void apagar(){
        System.out.println("El "+nombre+" "+marca+" "+referencia+" "+calidad+" se ha apagado");
    }
    
    public String toString(){
        return this.nombre+" "+this.marca+" "+this.referencia+" "+this.calidad+" a tan solo "+this.precio;
    }
}
