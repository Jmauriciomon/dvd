/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test_dvd;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class Test_DVD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        DVD dvd = new DVD();
        Scanner leer = new Scanner(System.in);
        int opcion;
        
        System.out.println("Prueba DVD");
        do{
            System.out.println("Menú de opciones");
            System.out.println("Para encender el DVD presione 1");
            System.out.println("Para abrir la bandeja presione 2");
            System.out.println("Para cerrar la bandeja presione 3");
            System.out.println("Para reproducir presione 4");
            System.out.println("Para pausar presione 5");
            System.out.println("Para adelantar presione 6");
            System.out.println("Para retroceder presione 7");
            System.out.println("Para apagar el DVD presione 8");
            
            opcion=leer.nextInt();
            
            switch(opcion){
            case 1: 
                dvd.prender();
                break;
            case 2: 
                dvd.abrir();
                break;
            case 3: 
                dvd.cerrar();
                break;
            case 4:
                dvd.reproducir();
                break;
            case 5:
                dvd.pausar();
                break;    
            case 6:
                dvd.adelantar();
                break;
            case 7:
                dvd.retroceder();
                break;
            case 8:
                dvd.apagar();
                break;
        }
        }while(opcion!=8);
        
    }
    
}
